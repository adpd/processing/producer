import { $ } from 'zx'
import * as fs from 'fs'
import { Queue } from 'bullmq';
import { Redis } from "ioredis"
import { exit } from 'process';

const NB_TEST_JOBS = 40

const testJobs = []
const realJobs = []
const jobs = realJobs

for (let i = 0; i < NB_TEST_JOBS; i++) {
  const cmd = ['sleep', Math.floor(Math.random() * 4) + 1]
  testJobs.push({
    name: `${cmd[0]} ${cmd[1]}`,
    data: {
      cmd: 'sleep',
      args: [cmd[1]]
    }
  })
}

const subjects = fs.readFileSync('subjects.txt').toString().trim().split("\n");

for (let subject of subjects) {
  realJobs.push(
    { 
      name: `patient ${subject}`,
      data: {
        cmd: '/home/christina/ANTs/install/bin/antsRegistrationSyN.sh',
        args: `-d 3 -f /home/christina/ADNI_AD_Bet_T1/sub-${subject}__t1_bet.nii.gz -m /home/christina/template/template_mni_icbm152_t1_tal_nlin_sym_09a_brain.nii -o /home/christina/AntsRegistration_Cammoun_AD_All/sub-${subject}_`.split(' ')
      }
    }
  )
}

const connection = new Redis({
  port: 6379,
  host: "localhost"
});

const myQueue = new Queue('ants', { connection });

jobs.forEach(async job => {
  const { name, data } = job
  console.log(`queueing job: ${name}`)
  await myQueue.add(name, data)
})
